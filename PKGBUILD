# Maintainer: OpenSorcerer <alex at opensourcery dot eu>
pkgname=tobii-eyetracker-driver
_pkgname=tobii_eye_tracker_linux_installer
pkgver=1.0
pkgrel=1
pkgdesc="Tobii EyeTracker driver and config utils, compatible with IS4 (Tobii 4C)"
arch=('x86_64')
url="https://github.com/MoscowskyAnton/$_pkgname"
license=('custom')
depends=(# tobiiusbservice
         'udev'
         # tobii-engine
         'sqlcipher'
         # tobii-config
         'nss' 'gconf' 'gtk2' 'libxtst' 'libxss'
         # TobiiPro Manager
         'gtk3')
makedepends=('git')
#install="$pkgname.install"
source=("git+$url.git"
        "tobiiusbservice.service"
        "tobiiusb.conf")
sha256sums=('SKIP'
            '7a0f87fd75308074e2534411cb03d2438322718c48e67c1f49c88537e51c9702'
            '882333dafd26910e2160301a9c3fa7ba491a5aa213750075e99f73b20e9611fc')

prepare() {
    cd "$_pkgname"
    
    # extract all archives
    mkdir -p "$srcdir/tobiiusbservice/data/" && bsdtar -xf tobiiusbservice_l64U14_2.1.5-28fd4a.deb -C "$srcdir/tobiiusbservice/" && bsdtar -xf "$srcdir/tobiiusbservice/data.tar.gz" -C "$srcdir/tobiiusbservice/data/"
    mkdir -p "$srcdir/tobii-engine/data/" && bsdtar -xf tobii_engine_linux-0.1.6.193_rc-Linux.deb -C "$srcdir/tobii-engine/" && bsdtar -xf "$srcdir/tobii-engine/data.tar.gz" -C "$srcdir/tobii-engine/data/"
    mkdir -p "$srcdir/tobii-config/data/" && bsdtar -xf tobii_config_0.1.6.111_amd64.deb -C "$srcdir/tobii-config/" && bsdtar -xf "$srcdir/tobii-config/data.tar.xz" -C "$srcdir/tobii-config/data/"
    mkdir -p "$srcdir/stream-engine/" && bsdtar -xf stream_engine_linux_3.0.4.6031.tar.gz -C "$srcdir/stream-engine/"
    mkdir -p "$srcdir/eyetracker-manager/data/" && bsdtar -xf TobiiProEyeTrackerManager-2.1.0.deb -C "$srcdir/eyetracker-manager/" && bsdtar -xf "$srcdir/eyetracker-manager/data.tar.xz" -C "$srcdir/eyetracker-manager/data/"
}

package() {
    # place tobiiusbservice files
    cd "$srcdir/tobiiusbservice/data/"
    install -Dm644 etc/udev/rules.d/99-tobiiusb.rules "$pkgdir/etc/udev/rules.d/99-tobiiusb.rules"
    install -Dm755 usr/local/lib/tobiiusb/libtobii_libc.so "$pkgdir/usr/local/lib/tobiiusb/libtobii_libc.so"
    install -Dm755 usr/local/lib/tobiiusb/libtobii_osal.so "$pkgdir/usr/local/lib/tobiiusb/libtobii_osal.so"
    install -Dm755 usr/local/lib/tobiiusb/libtobii_usb.so "$pkgdir/usr/local/lib/tobiiusb/libtobii_usb.so"
    install -Dm755 usr/local/sbin/tobiiusbserviced "$pkgdir/usr/local/sbin/tobiiusbserviced"
    install -Dm644 "$srcdir/tobiiusbservice.service" "$pkgdir/usr/lib/systemd/system/tobiiusbservice.service"
    install -Dm644 "$srcdir/tobiiusb.conf" "$pkgdir/etc/ld.so.conf.d/tobiiusb.conf"

    # place tobii-engine files
    cd "$srcdir/tobii-engine/data/"
    install -Dm644 etc/systemd/system/tobii_engine.service "$pkgdir/etc/systemd/system/tobii_engine.service"
    install -Dm777 usr/share/tobii_engine/db/config.db "$pkgdir/usr/share/tobii_engine/db/config.db"
    chmod 666 "$pkgdir/usr/share/tobii_engine/db"
    install -Dm644 -t "$pkgdir/usr/share/tobii_engine/firmware/" usr/share/tobii_engine/firmware/*
    install -Dm644 -t "$pkgdir/usr/share/tobii_engine/lib/" usr/share/tobii_engine/lib/*
    install -Dm644 -t "$pkgdir/usr/share/tobii_engine/model/" usr/share/tobii_engine/model/*
    install -Dm644 usr/share/tobii_engine/platform_modules/libplatmod_is4.sig "$pkgdir/usr/share/tobii_engine/platform_modules/libplatmod_is4.sig"
    install -Dm644 usr/share/tobii_engine/platform_modules/libplatmod_is4.so "$pkgdir/usr/share/tobii_engine/platform_modules/libplatmod_is4.so"
    install -Dm644 usr/share/tobii_engine/platform_modules/libplatmod_pdk.so "$pkgdir/usr/share/tobii_engine/platform_modules/libplatmod_pdk.so"
    install -Dm644 usr/share/tobii_engine/ssl/cert.pem "$pkgdir/usr/share/tobii_engine/ssl/cert.pem"
    install -Dm644 usr/share/tobii_engine/ssl/key.pem "$pkgdir/usr/share/tobii_engine/ssl/key.pem"
    install -Dm400 usr/share/tobii_engine/se_license_key_tobii_engine "$pkgdir/usr/share/tobii_engine/se_license_key_tobii_engine"
    install -Dm755 usr/share/tobii_engine/tobii_engine "$pkgdir/usr/share/tobii_engine/tobii_engine"

    # place tobii-config files
    cd "$srcdir/tobii-config/data/"
    install -Dm644 -t "$pkgdir/opt/tobii_config/locales/" opt/tobii_config/locales/*
    install -Dm644 -t "$pkgdir/opt/tobii_config/resources/resources/graphics/" opt/tobii_config/resources/resources/graphics/*
    install -Dm755 opt/tobii_config/resources/resources/libtobii_stream_engine.so "$pkgdir/opt/tobii_config/resources/resources/libtobii_stream_engine.so"
    install -Dm644 opt/tobii_config/resources/resources/se_license_key_tobii_config "$pkgdir/opt/tobii_config/resources/resources/se_license_key_tobii_config"
    install -Dm644 opt/tobii_config/resources/app.asar "$pkgdir/opt/tobii_config/resources/app.asar"
    install -Dm644 opt/tobii_config/resources/electron.asar "$pkgdir/opt/tobii_config/resources/electron.asar"
    install -Dm644 -t "$pkgdir/opt/tobii_config/" opt/tobii_config/*.*
    chmod 755 "$pkgdir/opt/tobii_config/libnode.so"
    install -Dm755 opt/tobii_config/tobii_config "$pkgdir/opt/tobii_config/tobii_config"
    install -Dm644 usr/share/applications/tobii_config.desktop "$pkgdir/usr/share/applications/tobii_config.desktop"
    install -Dm644 usr/share/doc/tobii-config/changelog.gz "$pkgdir/usr/share/doc/tobii-config/changelog.gz"
    for res in 16x16 24x24 32x32 48x48 64x64 96x96 128x128 256x256; do
        install -Dm755 usr/share/icons/hicolor/$res/apps/tobii_config.png "$pkgdir/usr/share/icons/hicolor/$res/apps/tobii_config.png"
    done

    # place stream-engine files
    cd "$srcdir/stream-engine/"
    install -Dm755 lib/x64/libtobii_stream_engine.so "$pkgdir/usr/lib/tobii/libtobii_stream_engine.so"
    install -Dm644 "$srcdir/$_pkgname/tobii.conf" "$pkgdir/etc/ld.so.conf.d/tobii.conf"
    install -Dm644 -t "$pkgdir/include/tobii/" include/tobii/*

    # place eyetracker-manager files
    cd "$srcdir/eyetracker-manager/data/"
    install -Dm644 -t "$pkgdir/opt/TobiiProEyeTrackerManager/locales/" opt/TobiiProEyeTrackerManager/locales/*
    install -Dm644 opt/TobiiProEyeTrackerManager/resources/stage/sdk/libraries/lib/libtobii_firmware_upgrade.so "$pkgdir/opt/TobiiProEyeTrackerManager/resources/stage/sdk/libraries/lib/libtobii_firmware_upgrade.so"
    install -Dm644 opt/TobiiProEyeTrackerManager/resources/stage/sdk/libraries/lib/libtobii_research.so "$pkgdir/opt/TobiiProEyeTrackerManager/resources/stage/sdk/libraries/lib/libtobii_research.so"
    install -Dm644 opt/TobiiProEyeTrackerManager/resources/stage/sdk/licenses/se_license_key_ETM_process "$pkgdir/opt/TobiiProEyeTrackerManager/resources/stage/sdk/licenses/se_license_key_ETM_process"
    install -Dm644 opt/TobiiProEyeTrackerManager/resources/ws/cert.pem "$pkgdir/opt/TobiiProEyeTrackerManager/resources/ws/cert.pem"
    install -Dm644 opt/TobiiProEyeTrackerManager/resources/ws/key.pem "$pkgdir/opt/TobiiProEyeTrackerManager/resources/ws/key.pem"
    install -Dm644 opt/TobiiProEyeTrackerManager/resources/app.asar "$pkgdir/opt/TobiiProEyeTrackerManager/resources/app.asar"
    install -Dm644 opt/TobiiProEyeTrackerManager/resources/electron.asar "$pkgdir/opt/TobiiProEyeTrackerManager/resources/electron.asar"
    install -Dm755 opt/TobiiProEyeTrackerManager/swiftshader/libEGL.so "$pkgdir/opt/TobiiProEyeTrackerManager/swiftshader/libEGL.so"
    install -Dm755 opt/TobiiProEyeTrackerManager/swiftshader/libGLESv2.so "$pkgdir/opt/TobiiProEyeTrackerManager/swiftshader/libGLESv2.so"
    install -Dm644 -t "$pkgdir/opt/TobiiProEyeTrackerManager/" opt/TobiiProEyeTrackerManager/*.*
    install -Dm755 opt/TobiiProEyeTrackerManager/tobiiproeyetrackermanager "$pkgdir/opt/TobiiProEyeTrackerManager/tobiiproeyetrackermanager"
    chmod 755 "$pkgdir"/opt/TobiiProEyeTrackerManager/*.so
    install -Dm644 usr/share/applications/tobiiproeyetrackermanager.desktop "$pkgdir/usr/share/applications/tobiiproeyetrackermanager.desktop"
    install -Dm644 usr/share/doc/tobiiproeyetrackermanager/changelog.gz "$pkgdir/usr/share/doc/tobiiproeyetrackermanager/changelog.gz"
    install -Dm644 usr/share/icons/hicolor/0x0/apps/tobiiproeyetrackermanager.png "$pkgdir/usr/share/icons/hicolor/0x0/apps/tobiiproeyetrackermanager.png"
}
